<?php

namespace prcssr;

/*
// обычный запрос
$url = "https://192.168.56.101:1500/ispmgr?authinfo=root:123456&out=JSONdata&func=domain";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//curl_setopt ($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
$r = curl_exec($ch);
if ($r === FALSE) {
    die(curl_error($ch));
}

curl_close($ch);

echo $r;
*/


class IspMgrDomain {
    
    public $out = "JSONdata"; // xml и т.д.
    
    public $port = 1500;
    
    public function __construct($user, $pass, $host) {
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
    }
    
    public function query($url_ending) {
        $url = $this->host . "?";
        $url .= "authinfo=" . $this->user . ":" . $this->pass . "&out=" . $this->out ."&func=";
        $url .=  $url_ending;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //curl_setopt ($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $r = curl_exec($ch);

        if ($r === FALSE) {
            die(curl_error($ch));
        }

        curl_close($ch);

        return $r;
    }
    
    public function domain() {
        $url = "domain";
        return $this->query($url);
    }
    
    /* Новое доменное имя
     * информация - sudo /usr/local/mgr5/sbin/mgrctl -m ispmgr -i domain.edit lang=ru (но в руководстве говорят см с лога)
     * пробел заменять на %20, - заменять на %2D (не обязательно)
     * (в логе отображаються get запросы - /usr/local/mgr5/var/ispmgr.log)
     */
     
    /* dtype = мастер для работы с записями, коммент выше для предыщуей версии скрипта */
    public function domainEdit($name, $masterip, $dtype) {
        $url = "domain.edit&sok=ok&name=${name}&dtype=${dtype}&masterip=${masterip}";
        return $this->query($url);
    }
    
    /* удалить домен */
    public function domainDelete($name) {
        $url = "domain.delete&elid=${name}";
        return $this->query($url);
    }
    
    /* Управление записями домена - прочитать */
    public function domainRecord($name) {
        $url = "domain.record&elid=${name}";
        return $this->query($url);
    }
    
    /* Управление записями домена - изменить 
     * @param Array $params массив ключь->значение с переменными
     */
    public function domainRecordEdit($params) {
        $url = "domain.record.edit&sok=ok";
        foreach ($params as $k => $v) {
            $url .= "&${k}=${v}";
        }
        //echo $url;
        return $this->query($url);
    }
    
    
}

